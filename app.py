from datetime import date
from statistics import mean
import os

import falcon
from falcon import media
import requests
from dateutil import relativedelta

API_KEY = os.environ.get('APIKEY', 'demo')
NDAYS = int(os.environ.get('NDAYS', 7))
TICKER = os.environ.get('SYMBOL', 'MSFT')
AVG_ON_KEY = '4. close'
TIME_SERIES_KEY = 'Time Series (Daily)'

PARAMS = {
        'apikey': API_KEY,
        'function': 'TIME_SERIES_DAILY_ADJUSTED',
        'symbol': TICKER
}


def get_ticker_closing_quotes():
    data = requests.get('https://www.alphavantage.co/query', params=PARAMS)
    if data.status_code != 200:
        return ValueError
    return data.json()[TIME_SERIES_KEY]


def filter_avg_over_ndays(data):
    # API docs doesn't seem to indicate guaranteed ordering:
    # > The most recent data point is the prices and volume information of the current trading day, updated realtime.
    # This is a bit brute force, I would actually calculate the NDAYS to get the
    # first date then use an rrule from dateutil to gather weekdays only.
    # I've added a buffer of 4 days to capture any potential gaps.
    # If the ordering is actually always right - then I would use reparse the raw body to an `collections.OrderedDict`
    closing_values = []
    for i in range(NDAYS + 4):
        target_date = str(date.today() -relativedelta.relativedelta(days=i))
        try:
            closing_values.append(float(data[target_date][AVG_ON_KEY]))
        except KeyError as e:
            continue
        if len(closing_values) == NDAYS:
            return closing_values, mean(closing_values)


class ClosingQuoteResource:

    def on_get(self, req, resp):
        try:
            quote_data = get_ticker_closing_quotes()
        except ValueError:
            falcon.HTTPError(400, title='couldnt reach quote service')
        raw_series_data, raw_series_average = filter_avg_over_ndays(quote_data)
        series_average = f'{raw_series_average:.2f}'
        series_data = [f'{i:.2f}'for i in raw_series_data]
        resp.media = {
            TICKER: {
                'data': series_data,
                'average': series_average
            }
        }


class NegotiationMiddleware:

    def process_request(self, req, resp):
        resp.content_type = req.accept


class QuoteTextHandler(media.BaseHandler):

    def serialize(self, media, content_type):
        try:
            for ticker in media.keys():
                data_str = ','.join(media[ticker]['data'])
                average = media[ticker]['average']
            return f'{TICKER} data=[{data_str}], average={average}'.encode('utf-8')
        except KeyError:
            return str(media).encode('utf-8')

    # required due to media.BaseHandler being and AbstractBaseClass
    def deserialize(self, stream, content_type, content_length):
        pass


extra_handlers = media.Handlers({
        'text/plain': QuoteTextHandler()
})

api = falcon.API(middleware=NegotiationMiddleware())
api.add_route('/', ClosingQuoteResource())
api.resp_options.media_handlers.update(extra_handlers)
