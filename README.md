# avantage

Proxy Alpha Vantage service so that returns closing prices over N-GIVEN days. Provide the average over N-GIVEN days.


quote query example:

    https://www.alphavantage.co/query?apikey=demo&function=TIME_SERIES_DAILY_ADJUSTED&symbol=MSFT

avantage expected format:

```
MSFT  data=[110.56, 111.25, 115.78], average=112.50
```

Getting the requested format requires proper content type negotiation, otherwise the service defaults to `application/json`. I didn't get around to moving middle ware to default to `text/plain`

```
❯ curl -H 'Accept: text/plain' localhost:8000/
MSFT data=[133.96,134.15,133.93,133.43,137.78], average=134.65
```


# getting started

## prereqs

* helm installed
* minikube or a cluster with an ingress controller.


## install helm package

commented out variables show defaults and an example install different than defaults. I've disabled health checks (in the package) to prevent abusive behavior to the api.

```
# basic install
export APIKEY=supersecret

# bare minimal
helm install --set=secret.APIKEY="${APIKEY}" \
             https://grumps.gitlab.io/avantage/avantage-1.0.0.tgz

# new ticker and more history:
helm install --set=secret.APIKEY="${APIKEY}" \
      --set=env.SYMBOL=TSLA \
      --set=env.NDAYS=20 \
      --set=image.tag=v0.0.2 \
      https://grumps.gitlab.io/avantage/avantage-1.0.0.tgz
# full opts and defaults
# helm install --set=secret.APIKEY="${APIKEY}" \
#       --set=env.SYMBOL=MSFT \
#       --set=env.NDAYS=7 \
#       --set=image.tag=v0.0.2 \
#       --set=ingress.hosts.host=avantage.local \
#       --set=ingress.annotations={} # if your controller needs an annotation \
#       --set=service.port=80 # defaults to "advantage.local"  \
```


This was testing with ingress-nginx on minikube and on Digital Ocean with a LoadBalancer type as well. _Note: minikube on linux with kvm2 engine, I don't have a mac_

I get my ip from minikube via
```
minikube ip
```

Then update my `/etc/hosts` with the minikube ip address and the hostname `avantage.local`

Now with the proper accept content-type you'll get the format:

```
❯ curl -H 'Accept: text/plain' http://avantage.local/
MSFT data=[133.96,134.15,133.93,133.43,137.78], average=134.65

❯ curl http://avantage.local/
{"MSFT": {"data": ["133.96", "134.15", "133.93", "133.43"], "average": "133.87"}}%
```


If you prefer to just run the container:
```
docker run -p 8000:8000 registry.gitlab.com/grumps/avantage:v0.0.2
```

# app code

* The service doesn't request the full history, so it only goes far back as the service does by default
* if the demo key is used it will eventually start to flap (crashloop)

