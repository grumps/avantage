FROM python:3.7.3-alpine as build
COPY . .
RUN mkdir -p /opt/wheelhouse && pip install wheel && pip wheel -r reqs/reqs.txt --wheel-dir=/opt/wheelhouse

FROM python:3.7.3-alpine as prod
RUN adduser -S -D -H -u 1000 app && mkdir -p /opt/{avantage,wheelhouse}
COPY --from=build /opt/wheelhouse /opt/wheelhouse
COPY app.py reqs/* /opt/avantage/
WORKDIR /opt/avantage
RUN pip install --no-index \
                --find-links=/opt/wheelhouse \
                -r /opt/avantage/reqs.txt \
                && chown app /opt/avantage \
                && rm -rf /opt/wheelhouse
USER app
ENTRYPOINT ["gunicorn"]
CMD ["-b", "0.0.0.0:8000", "-w", "4", "app:api"]
