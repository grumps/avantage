#!/usr/bin/env make
VER:=$(shell cat VERSION)

run: build
	docker run -p 8000:8000 avantage

build:
	docker build -t avantage .

push: build
	docker tag avantage registry.gitlab.com/grumps/avantage:${VER}
	docker push registry.gitlab.com/grumps/avantage:${VER}


